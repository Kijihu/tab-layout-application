package com.example.homeworkfacebook.help;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentHelper {

    static public void addFragment(AppCompatActivity appCompatActivity, int container, Fragment fragment){

        FragmentManager fm = appCompatActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(container, fragment);
        ft.commit();

    }

    static public void replaceFragment(AppCompatActivity appCompatActivity, int container, Fragment fragment){

        FragmentManager fm = appCompatActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(container, fragment);
        ft.commit();

    }
}
