package com.example.homeworkfacebook;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class AppPaggerAdapter extends FragmentPagerAdapter {

    private int tabCount;
    private CharSequence[] tabTitle = {"1","1","1","1","1"};

    public AppPaggerAdapter(@NonNull FragmentManager fm, int tabCount) {
        super(fm, tabCount);
        this.tabCount = tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new TabHome();
                break;
            case 1:
                fragment = new TabVideo();
                break;
            case 2:
                fragment = new TabAccount();
                break;
            case 3:
                fragment = new TabNotification();
                break;
            case 4:
                fragment = new TabReorder();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitle[position];
    }
}
