package com.example.homeworkfacebook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TableLayout;

import com.example.homeworkfacebook.help.FragmentHelper;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private TabLayout appTab;
    private ViewPager tabBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initDefaultTabBody();
//        configureTab();
        configureViewPagger();
    }

    private void initView() {
        appTab = findViewById(R.id.main_tl_menu);
        tabBody = findViewById(R.id.main_fl_body);
    }

    private void initDefaultTabBody(){
        FragmentHelper.addFragment(this,R.id.main_fl_body, new TabHome());
    }

    private void configureTab(){
        appTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                Fragment fragment = null;
                switch (position){
                    case 0:
                        fragment = new TabHome();
                        break;
                    case 1:
                        fragment = new TabVideo();
                        break;
                    case 2:
                        fragment = new TabAccount();
                        break;
                    case 3:
                        fragment = new TabNotification();
                        break;
                    case 4:
                        fragment = new TabReorder();
                        break;
                }
                FragmentHelper.replaceFragment(MainActivity.this,R.id.main_fl_body,fragment);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void configureViewPagger(){
        AppPaggerAdapter appPaggerAdapter = new AppPaggerAdapter(
                getSupportFragmentManager(), appTab.getTabCount()
        );
        appTab.setupWithViewPager(tabBody);
        tabBody.setAdapter(appPaggerAdapter);
    }
}
